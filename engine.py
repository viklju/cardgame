#!/usr/bin/env python3
from time import sleep as t_sleep
from random import choice as r_choice
from cards import deck
from cards import monsters  # = [bat, slime, skeleton, zombie, firebat]
# import pygame  # would like to have a graphical interface
"""Game engine, meant to manage dealing cards, abilities, monsters and so on.
Should be executed by the user, WORK IN PROGRESS"""


def printer(card1='', card2='', card3='', opponents=['', '', '', ''], o_stats=['', '', '', '']):
    opponents = [opponent.center(10) for opponent in opponents]
    o_stats = [o_stat.center(10) for o_stat in o_stats]

    # border
    print('+' + '-' * 200 + '+')

    # border spacing
    for i in range(5):
        print('|' + ' ' * 200 + '|')

    # print opponent cards
    print('|' + ' ' * 120 + '+' + '-' * 12 + '+' + ' ' * 3 + '+' + '-' * 12 + '+' + ' ' * 3 + '+' + '-' * 12 + '+' + ' ' * 3 + '+' + '-' * 12 + '+' + ' ' * 15 + '|')
    for i in range(8):
        print('|' + ' ' * 120 + '|' + ' ' * 12 + '|' + ' ' * 3 + '|' + ' ' * 12 + '|' + ' ' * 3 + '|' + ' ' * 12 + '|' + ' ' * 3 + '|' + ' ' * 12 + '|' + ' ' * 15 + '|')
    # opponent name
    print('|' + ' ' * 120 + '| {0} |   | {1} |   | {2} |   | {3} |'.format(opponents[0], opponents[1], opponents[2], opponents[3]) + ' ' * 15 + '|')
    # end card
    print('|' + ' ' * 120 + '+' + '-' * 12 + '+' + ' ' * 3 + '+' + '-' * 12 + '+' + ' ' * 3 + '+' + '-' * 12 + '+' + ' ' * 3 + '+' + '-' * 12 + '+' + ' ' * 15 + '|')
    # opponent statline
    print('|' + ' ' * 120 + '  {0}       {1}       {2}       {3}  '.format(o_stats[0], o_stats[1], o_stats[2], o_stats[3]) + ' ' * 15 + '|')

    # room for hand and played cards
    for i in range(20):
        print('|' + ' ' * 200 + '|')

    # border
    print('+' + '-' * 200 + '+')


def remove_dead():
    pass

clear = lambda: os_system('cls')


try:
    bat1 = monsters[0]
    bat1_inf = bat1.get_info()
    mon1 = r_choice(monsters)
    mon1_inf = mon1.get_info()
    printer(opponents=[bat1_inf['name'], mon1_inf['name'], '', ''], o_stats=[str(bat1_inf['statline']['Attack']) + '/' + str(bat1_inf['statline']['Defence']), str(mon1_inf['statline']['Attack']) + '/' + str(mon1_inf['statline']['Defence']), '', ''])
    bat1.damage(3)
    # clear()
    printer(opponents=[bat1_inf['name'], mon1_inf['name'], '', ''], o_stats=[str(bat1_inf['statline']['Attack']) + '/' + str(bat1_inf['statline']['Defence']), str(mon1_inf['statline']['Attack']) + '/' + str(mon1_inf['statline']['Defence']), '', ''])

    # card1 = deck.draw()
    # if card1.get_type() == 'creature':
    #     print(card1.get_info()['statline'])
    #     card1.battle(monsters[0])
    #     print('-' * 10)
    #     print(monsters[0].get_info()['statline'])
    #     monsters[0].reset()
    #     print('-' * 10)
    #     print(monsters[0].get_info()['statline'])
    # else:
    #     print('try again')

    # deal hand
    # for i in range(5):
    #     print(deck.draw().get_info())
    #
    # # divider
    # print('-' * 40)
    #
    # # monsters
    # print(monsters[0].get_info())
    # print(r_choice(monsters).get_info())


except Warning as message:
    print(message)
