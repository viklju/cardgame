#!/usr/bin/env python3
from random import shuffle as r_shuffle
# shuffle sometimes cashes the "randomness", random.SystemRandom might fix this
"""Defines objects; Card, Ability and Deck. Intended for use in "cards.py".
"""


class Card:
    """Creatures, spells (and monsters) are all cards. All cards must later be
    defined with a type using the function "creature" or "spell", though never
    both. Cards can have any number of abilities added to them, by passing an
    ability from the "Ability"-object to the "add_ability" function below.
    """

    def __init__(self, image='place-holder'):
        """WORK IN PROGRESS. Defines card art and some internal variables.
        """
        self.__info__       = {}     # dictionary of all defined variables
        self.__type__       = None   # card type, either creature or spell
        self.__ability__    = []     # list of card abilities
        self.__image__      = image  # card art, WORK IN PROGRESS
        self.__base_stats__ = None   # used when resetting creatures statline

    def __type_check__(self, type=None):
        """Enforces card type. Raises a warning if the card-type is not type.
        Once a card is given a type it cannot be changed without recreating
        the card.
        """
        if self.__type__ is not type:
            raise Warning('Card defined as: ' + str(self.__type__) + ', operation failed')

    def creature(self, name, attack=0, defence=0, cost=0, flavor=''):
        """Defines the card type as a creature, with a name, cost, statline
        (attack and defence) and cosmetic flavortext.
        """
        self.__type_check__(None)  # check if card type is already defined
        self.__type__       = 'creature'
        self.__name__       = name
        self.__cost__       = cost
        self.__statline__   = {'Attack': attack, 'Defence': defence}
        self.__base_stats__ = {'Attack': attack, 'Defence': defence}
        self.__flavor__     = flavor
        self.__info__.update({
            'type':     self.__type__,
            'name':     self.__name__,
            'cost':     self.__cost__,
            'statline': self.__statline__,
            'text':     self.__flavor__
            })

    def spell(self, name, cost=0, flavor=''):
        """Defines the card type as a spell, with a name, cost and flavortext.
        """
        self.__type_check__(None)  # check if card type is already defined
        self.__type__   = 'spell'
        self.__name__   = name
        self.__cost__   = cost
        self.__flavor__ = flavor
        self.__info__.update({
            'type': self.__type__,
            'name': self.__name__,
            'cost': self.__cost__,
            'text': self.__flavor__
            })

    def add_ability(self, ability):
        """Adds an ability to the card, WORK IN PROGRESS. Should only be passed
        abilities from the "Ability"-object, though no checks are in place.
        """
        self.__ability__.append(ability)
        self.__info__.update({'ability': self.__ability__})

    def damage(self, amount):
        """Reduces the card defence-stat, e.g. battle.
        """
        self.__statline__['Defence'] -= abs(amount)

    def battle(self, defender):
        """Deals damage to a defending card based on the attackers statline,
        between two creature cards.
        """
        # perhaps not the most pythonic solution, using __type_check__ for the defender
        self.__type_check__('creature')      # check if attacker is creature
        defender.__type_check__('creature')  # check if defender is creature
        defender.damage(defender.__statline__['Defence'] - self.__statline__['Attack'])

    def reset(self):
        """Resets a creature-card statline to the base stats, as defined in
        "creature"-function.
        """
        self.__type_check__('creature')
        self.__info__['statline'], self.__statline__ = self.__base_stats__, self.__base_stats__

    def get_info(self):
        """Returns a dictionary of all defined variables of the card.
        """
        return self.__info__

    def get_type(self):
        """Returns the card type.
        """
        return self.__type__


class Ability:
    """Card Ability-object, WORK IN PROGRESS. Currently only adds descriptive
    strings using functions below. Should be passed to the "add_ability"-
    function in the "Card"-object.
    """

    def draw(n=1):
        return 'Draw ' + str(n) + ' card(s)'

    def discard(n=1):
        return 'Discard ' + str(n) + ' card(s)'

    def look(n=1):
        return 'Look at ' + str(n) + ' card(s), from the top'

    def reorder(n=2):
        return 'look at ' + str(n) + ' cards, you may put them back in any order'

    def charge():
        return 'no summoning sickness'


class Deck():
    """Deck-object; intended to create and manage a deck of cards from the
    "Card"-object.
    """

    def __init__(self):
        self.__collection__ = []  # list of cards (deck)
        self.__deck_limit__ = 15  # maximum amount of cards in a deck

    def number_of_cards(self):
        """Returns the number of cards in a deck.
        """
        return len(self.__collection__)

    def add(self, card, amount=1):
        """Adds one or more cards to a deck. Cards should be cards with
        card-type creature or spell from the "Card"-object. Checks if the
        addition would bring the deck past the deck limit. Also has a card-type
        validation check.
        """
        if (self.number_of_cards() + amount) <= self.__deck_limit__:
            if card.get_type() == 'creature' or card.get_type() == 'spell':
                for times in range(amount):
                    self.__collection__.append(card)
            else:
                raise Warning('Tried to add an incomplete card to deck, could not find a card type')
        else:
            raise Warning('Tried to add to many cards to deck\n A deck is only', self.__deck_limit__, 'cards!')

    def remove(self, card):
        """Removes the first occurence of a card from the deck.
        """
        self.__collection__.remove(card)

    def shuffle(self):
        """With the module "random" it shuffles the list of cards (deck).
        """
        r_shuffle(self.__collection__)
        return self.__collection__

    def draw(self):
        """Returns the top card of the deck and removes it from the deck.
        """
        return self.__collection__.pop(0)

    def look(self, amount=1):
        """Returns one or more cards from the top of the deck, without removing
        them from the deck. WORKING IN PROGRESS.
        """
        return self.__collection__[0:amount]

    def all_cards(self):
        """Returns all the cards in a deck.
        """
        # return self.__collection__  # alternatively
        return [card.get_info()['name'] for card in self.__collection__]
