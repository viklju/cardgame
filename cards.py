#!/usr/bin/env python3
from classes import Card
from classes import Ability
from classes import Deck
"""Creates a bunch of cards and puts them in a deck. Also creates a list of
monsters. Intended for use in "engine.py"."""

try:
    # monsters:
    bat, slime, skeleton, zombie, firebat = Card(), Card(), Card(), Card(), Card()
    bat.creature('bat', attack=1, defence=1, cost=0, flavor='annoying bat')
    slime.creature('slime', attack=1, defence=4, cost=2, flavor='medium-size slime')
    skeleton.creature('skeleton', attack=3, defence=2, cost=3, flavor='time traveller')
    zombie.creature('zombie', attack=2, defence=3, cost=3, flavor='the living dead')
    firebat.creature('firebat', attack=5, defence=1, cost=2, flavor='bat in flames, not a good combo')
    monsters = (bat, slime, skeleton, zombie, firebat)

    # creature cards:
    priest, warrior, alchemist, mage, archer = Card(), Card(), Card(), Card(), Card()
    priest.creature('priest', attack=1, defence=3, cost=2, flavor='young priest')
    warrior.creature('warrior', attack=3, defence=4, cost=3, flavor='battle axe thrower')
    alchemist.creature('alchemist', attack=5, defence=2, cost=3, flavor='potions and poisons')
    mage.creature('mage', attack=7, defence=2, cost=4, flavor='fireball!')
    archer.creature('archer', attack=4, defence=3, cost=3, flavor='eagle eye')

    # spell cards:
    fireball, arcane_intellect, tactics = Card(), Card(), Card()
    fireball.spell('fireball', cost=3, flavor='ball of fire')
    fireball.add_ability(Ability.charge())
    # spells without abilities aren't very useful

    arcane_intellect.spell('arcance intellect', cost=3, flavor='secrets of magic')
    arcane_intellect.add_ability(Ability.look(3))
    arcane_intellect.add_ability(Ability.draw(2))

    tactics.spell('tactics', cost=2, flavor='battle planning')
    tactics.add_ability(Ability.reorder(3))
    tactics.add_ability(Ability.draw(1))

    # deck creation:
    deck = Deck()  # note: deck limit defined in "classes.py", default 15 cards
    deck.add(priest, 1)
    deck.add(warrior, 3)
    deck.add(alchemist, 2)
    deck.add(mage, 1)
    deck.add(archer, 2)
    deck.add(fireball, 1)
    deck.add(tactics, 2)
    deck.add(arcane_intellect, 3)
    deck.shuffle()  # don't forget to shuffle

except Warning as message:
    print(message)
