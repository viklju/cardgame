# Python Card Game
An unfishied attempt at a card game, written with proper documentation and error handeling.
Meant as practice to better understand and work with object oriented programming...

## Classes.py
Defines objects; Card, Ability and Deck. Intended for use in "cards.py".

## Cards.py
Creates a bunch of cards and puts them in a deck. Also creates a list of monsters. Intended for use in "engine.py".

## Engine.py
Game engine, meant to manage dealing cards, abilities, monsters and so on.
Should be executed by the user, WORK IN PROGRESS.

